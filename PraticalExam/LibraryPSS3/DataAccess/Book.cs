﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibraryPSS3.DataAccess
{
    public partial class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public string Genre { get; set; }
        public int? Price { get; set; }
    }
}

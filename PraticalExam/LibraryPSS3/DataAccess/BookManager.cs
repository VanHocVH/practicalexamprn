﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryPSS3.DataAccess
{
    public class BookManager
    {
        private static BookManager instance = null;
        private static readonly object instanceLock = new object();
        private BookManager() { }
        public static BookManager Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new BookManager();
                    }
                    return instance;
                }
            }
        }
        public Book GetBookById(int BookId)
        {
            Book book = null;
            try
            {
                var myBookDB = new BookManagementContext();
                book = myBookDB.Books.SingleOrDefault(book => book.Id == BookId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return book;
        }
        public void AddNew(Book book)
        {
            try
            {
                Book _book = GetBookById(book.Id);
                if (_book == null)
                {
                    var bookContext = new BookManagementContext();
                    bookContext.Books.Add(book);
                    bookContext.SaveChanges();
                }
                else
                {
                    throw new Exception("The car is already exist");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}


﻿
using LibraryPSS3.DataAccess;

namespace LibraryPSS3.Repositories
{
    public class BookRepository : IBookRepository
    {
        public void AddBook(Book book)
        {
            BookManager.Instance.AddNew(book);
        }

        public Book GetBookByID(int bookID) =>
            BookManager.Instance.GetBookById(bookID);
    }
}

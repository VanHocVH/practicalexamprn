﻿using LibraryPSS3.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryPSS3.Repositories
{
    public interface IBookRepository
    {
        Book GetBookByID(int bookID);
        void AddBook(Book book);
    }
}

﻿using LibraryPSS3.DataAccess;
using LibraryPSS3.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PraticalExam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IBookRepository bookRepository;
        public MainWindow(IBookRepository repository)
        {
            InitializeComponent();
            bookRepository = repository;
        }
        public Book GetBookObject()
        {
            Book book = null;
            try
            {
                book = new Book
                {
                    Title = txtBookTitle.Text,
                    Author = txtAuthor.Text,
                    Publisher = txtPulisher.Text,
                    Genre = txtGenre.Text,
                    Price = int.Parse(txtPrice.Text)
                };
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Book");
            }
            return book;
        }
        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Book book = GetBookObject();
                bookRepository.AddBook(book);
                MessageBox.Show($"{book.Title} inserted");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Insert Book");
            }
        }
    }
}

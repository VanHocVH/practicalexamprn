﻿using LibraryPSS3.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;

namespace PraticalExam
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;
        public App()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }
        private void ConfigureServices(ServiceCollection services)
        {
            services.AddSingleton(typeof(IBookRepository), typeof(BookRepository));
            services.AddSingleton<MainWindow>();
        }
        private void OnStartup (object sender,StartupEventArgs e)
        {
            var windowBook = serviceProvider.GetService<MainWindow>();
            MainWindow.Show();
        }
    }
}

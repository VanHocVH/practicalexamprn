﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LibraryPSS3.DataAccess;

namespace BookManagePage.Pages.Books
{
    public class IndexModel : PageModel
    {
        private readonly LibraryPSS3.DataAccess.BookManagementContext _context;

        public IndexModel(LibraryPSS3.DataAccess.BookManagementContext context)
        {
            _context = context;
        }

        public IList<Book> Book { get;set; }

        public async Task OnGetAsync()
        {
            Book = await _context.Books.ToListAsync();
        }
    }
}
